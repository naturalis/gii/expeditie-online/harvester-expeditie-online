<?php

class NSR extends BaseClass
{
    private $path;
    private $files;
    private $has_header = true;
    private $names = [];

    protected $table_name = "nsr";
    protected $table_def =
        "create table if not exists nsr (
            id                     INTEGER PRIMARY KEY  autoincrement,
            scientific_name        varchar(255),
            nomen                  varchar(255),
            common_names           text,
            inserted               timestamp not null,
            UNIQUE(scientific_name,common_names)
        );";

    protected $sql_insert = "
        insert into nsr (
            scientific_name, nomen, common_names, inserted
        ) values (
            :scientific_name, :nomen, :common_names, datetime('now')
        )";

    protected $job_name = "NSR";

    public function __construct ()
    {
        parent::__construct();

        $this->path = getEnv('HARVESTER_PATH_NSR') ?: null;

        if (empty($this->path))
        {
            throw new Exception("no input folder set" ,1);
        }

        $this->path = rtrim($this->path,"/") . "/";

        if (!file_exists($this->path))
        {
            throw new Exception("input folder " . $this->path . " doesn't exist" ,1);
        }

    }

    public function runImport()
    {
        $this->fetchFiles();
        $this->readFiles();

        if (count($this->names)>0)
        {
            $this->clearTable();
            $this->insertData();
        }

        $this->setJobResult([
            "files" => $this->files,
            "imported" => $this->imported
        ]);
    }

    private function fetchFiles()
    {
        $this->files =  preg_grep('/\.(csv|tsv)$/i', glob($this->path . '*'));
        $this->logger->log("found " . number_format(count($this->files)) . " files" );
    }

    private function readFiles()
    {
        $this->names = [];
        $sep ="\t";

        foreach ($this->files as $file)
        {
            $lines = file($file);

            if (!str_contains($lines[0], "\t"))
            {
                $sep = ",";
            }
            else
            {
                $sep = "\t";
            }

            $this->lines_extracted = 0;

            foreach($lines as $n => $line)
            {
                if ($n==0 && $this->has_header)
                {
                    continue;
                }

                if (!str_contains($file, "synonyms"))
                {
                    $this->extractFromMain($line,$sep);
                }
                else
                {
                    $this->extractFromSynonyms($line,$sep);
                }

            }

            $this->logger->log("read '" . basename($file) . "'" );
            $this->logger->log("found " . number_format($this->lines_extracted) . " names");
        }

        $this->names = array_unique($this->names,SORT_REGULAR);
    }

    private function extractFromMain($line,$sep)
    {
        $cells = str_getcsv($line,$sep);
        $sci_name = trim($cells[0]);
        $authorship = trim($cells[1]);
        $common_name = trim($cells[2]);
        $nomen = trim(str_replace($authorship,"", $sci_name));

        $this->names[$nomen] = [
            "scientific_name"=>$sci_name,
            "common_names"=>
                (!empty($common_name) ? [[ "name" => $common_name, "name_type" => "isPreferredNameOf", "language_code"=>"nl" ]] : []),
            "nomen" => $nomen,
        ];

        $this->lines_extracted++;
    }

    private function extractFromSynonyms($line,$sep)
    {
        $cells = str_getcsv($line,$sep);
        $sci_name = trim($cells[3]);
        $authorship = trim($cells[4]);
        $common_name = trim($cells[0]);
        $name_type = trim($cells[1]);
        $language = trim($cells[2]);

        if (!empty($sci_name) && !empty($common_name) && $name_type!="isPreferredNameOf")
        {
            $language_code=null;

             switch($language)
             {
                case "Dutch":
                    $language_code="nl";
                    break;
                case "English":
                    $language_code="en";
                    break;
                case "Afrikaans":
                    $language_code="af";
                    break;
                default:
                    $this->logger->log("hitherto unknown language: " . $language);
            }

            $matches = array_filter($this->names,function($a) use ($sci_name)
                {
                    return $a["scientific_name"]==$sci_name && !empty($a["nomen"]);
                });

            if ($matches)
            {
                $nomen = array_pop($matches)["nomen"];

                $this->names[$nomen]["common_names"][]=
                    [ "name" => $common_name, "name_type" => $name_type, "language_code" => $language_code ];
                $this->lines_extracted++;
            }
            else
            {
                // $nomen = null;
                $this->logger->log("synonym not in main file: " . $sci_name);
            }
        }
    }

    private function insertData()
    {
        $this->imported=0;
        $this->db->exec("begin transaction");
        foreach ($this->names as $name)
        {
            if (empty($name["common_names"]))
            {
                continue;
            }

            $stmt = $this->db->prepare($this->sql_insert);
            $stmt->bindValue(':scientific_name',$name["scientific_name"],SQLITE3_TEXT);
            $stmt->bindValue(':nomen',$name["nomen"],SQLITE3_TEXT);
            $stmt->bindValue(':common_names',json_encode($name["common_names"]),SQLITE3_TEXT);
            if ($stmt->execute()===false)
            {
                $this->logger->log(
                    "error " . $this->db->lastErrorCode() . ": \"" .
                    $this->db->lastErrorMsg() .
                    "\" - " .
                    $name["scientific_name"]);
            }
            $this->imported++;
        }
        $this->db->exec("commit");

        $this->logger->log("saved " . number_format($this->imported) . " names");
    }

}
