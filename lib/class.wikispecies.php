<?php

class WikiSpecies extends BaseClass
{
    private $path;
    private $files;
    private $has_header = true;
    private $names = [];

    protected $job_name = "wikispecies";
    protected $table_name = "wikispecies";
    protected $table_def =
        "create table if not exists wikispecies (
            id                     INTEGER PRIMARY KEY  autoincrement,
            scientific_name        varchar(255),
            common_name            varchar(255),
            language_code          varchar(2),
            inserted               timestamp not null,
            UNIQUE(scientific_name,common_name,language_code)
        );";

    protected $sql_insert = "
        insert into wikispecies (
            scientific_name, common_name, language_code, inserted
        ) values (
            :scientific_name, :common_name, :language_code, datetime('now')
        )";

    public function __construct ()
    {
        parent::__construct();

        $this->path = getEnv('HARVESTER_PATH_WIKISPECIES') ?: null;

        if (empty($this->path))
        {
            throw new Exception("no input folder set" ,1);
        }

        $this->path = rtrim($this->path,"/") . "/";

        if (!file_exists($this->path))
        {
            throw new Exception("input folder " . $this->path . " doesn't exist" ,1);
        }
    }

    public function runImport()
    {
        $this->fetchFiles();
        $this->readFiles();
        if (count($this->names)>0)
        {
            $this->clearTable();
            $this->insertData();
        }
        $this->setJobResult([
            "records" => $this->imported,
            "files" => $this->files
        ]);
    }

    // public function clearTable()
    // {
    //     $stmt = $this->db->prepare("delete from $this->table_name");
    //     $stmt->execute();
    //     $this->logger->log("cleared records" );
    // }

    private function fetchFiles()
    {
        $this->files =  preg_grep('/\.(csv|tsv)$/i', glob($this->path . '*'));
        $this->logger->log("found " . number_format(count($this->files)) . " files" );
    }


    private function extractFromMain($line,$sep)
    {
        $cells = str_getcsv($line,$sep);
        $sci_name = trim($cells[1]);
        $common_name = trim($cells[0]);

        if (!empty($common_name))
        {
            $this->names[] = ["scientific_name"=>$sci_name,"common_name"=>$common_name,"language_code"=>"nl"];
            $this->lines_extracted++;
        }
    }

    private function extractFromSynonyms($line,$sep)
    {
        $cells = str_getcsv($line,$sep);
        $sci_name = trim($cells[3]);
        $common_name = trim($cells[0]);
        $name_type = trim($cells[1]);
        $language = trim($cells[2]);

        if (!empty($sci_name) && !empty($common_name) && $name_type=="isAlternativeNameOf" && $language=="Dutch")
        {
            $this->names[] = ["scientific_name"=>$sci_name,"common_name"=>$common_name,"language_code"=>"nl"];
            $this->lines_extracted++;
        }
    }


    private function readFiles()
    {
        $this->names = [];
        $sep ="\t";

        foreach ($this->files as $file)
        {
            $lines = file($file);

            if (!str_contains($lines[0], "\t"))
            {
                $sep = ",";
            }
            else
            {
                $sep = "\t";
            }

            $this->lines_extracted = 0;

            foreach($lines as $n => $line)
            {
                if ($n==0 && $this->has_header)
                {
                    continue;
                }

                if (!str_contains($file, "synonyms"))
                {
                    $this->extractFromMain($line,$sep);
                }
                else
                {
                    $this->extractFromSynonyms($line,$sep);
                }

            }

            $this->logger->log("read '" . basename($file) . "'" );
            $this->logger->log("found " . number_format($this->lines_extracted) . " names");
        }

        $this->names = array_unique($this->names,SORT_REGULAR);
    }

    private function insertData()
    {
        $this->imported=0;
        $this->db->exec("begin transaction");
        foreach ($this->names as $name)
        {
            $stmt = $this->db->prepare($this->sql_insert);
            $stmt->bindValue(':scientific_name',$name["scientific_name"],SQLITE3_TEXT);
            $stmt->bindValue(':common_name',$name["common_name"],SQLITE3_TEXT);
            $stmt->bindValue(':language_code',$name["language_code"],SQLITE3_TEXT);
            $stmt->execute();
            $this->imported++;
        }
        $this->db->exec("commit");

        $this->logger->log("saved " . number_format($this->imported) . " names");
    }

}
