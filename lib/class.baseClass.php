<?php

class BaseClass
{
    protected $max_records = 1000000;
    protected $db;
    protected $imported = 0;
    protected $total = 0;
    protected $job_start;
    protected $log_table =
        "create table if not exists job_log (
            id             INTEGER PRIMARY KEY  autoincrement,
            job_type       varchar(255) not null,
            job_name       varchar(255) not null,
            job_result     text,
            job_started    timestamp not null,
            job_finished   timestamp not null
        );";

    private $sql_log = "
        insert into job_log (
            job_type, job_name, job_result, job_started, job_finished
        ) values (
            'harvest', :job_name, :job_result, :job_started, datetime('now')
        )";

    protected $job_result=[];

    public function __construct()
    {

        $this->logger = new Logger();

        $this->job_start = date('Y-m-d H:i:s');

        $this->db_path = getenv('DATABASE_PATH');

        if (empty($this->db_path))
        {
            throw new Exception("empty database path", 1);

        }
        if (!file_exists($this->db_path))
        {
            throw new Exception("database doesn't exist: $this->db_path", 1);

        }

        $this->db = new SQLite3($this->db_path);

        if (isset($this->table_def))
        {
            if (is_array($this->table_def))
            {
                foreach ($this->table_def as $val)
                {
                    $this->db->exec($val);
                }
            }
            else
            {
                $this->db->exec($this->table_def);
            }
        }

        $this->db->exec($this->log_table);
    }

    public function __destruct()
    {
        if ($this->db)
        {
            $this->db->close();
        }
    }

    final function setJobResult($job_result)
    {
        $this->job_result = $job_result;
    }

    public function setMaxRecords($max_records)
    {
        $this->max_records = $max_records;
    }

    public function getMaxRecords()
    {
        return $this->max_records;
    }

    public function logImport()
    {
        $stmt = $this->db->prepare($this->sql_log);
        $stmt->bindValue(':job_name',$this->job_name,SQLITE3_TEXT);
        $stmt->bindValue(':job_result',json_encode($this->job_result),SQLITE3_TEXT);
        $stmt->bindValue(':job_started',$this->job_start,SQLITE3_TEXT);
        $stmt->execute();
    }

    protected function clearTable()
    {
        if (is_array($this->table_name))
        {
            foreach ($this->table_name as $val)
            {
                $this->db->exec("delete from $val");
            }
        }
        else
        {
            $this->db->exec("delete from $this->table_name");
        }
    }

    protected function getLastJsonError()
    {
        $json_error = false;

        switch (json_last_error())
        {
            case JSON_ERROR_NONE:
            break;
            case JSON_ERROR_DEPTH:
                $json_error = 'Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $json_error = 'Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $json_error = 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                $json_error = 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                $json_error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            default:
                $json_error = 'Unknown error';
            break;
        }

        return $json_error;
    }

    protected function runMySQLImport()
    {
        $this->fetchMySqlFiles();
        $this->transMySqlformFiles();
        $this->runSqlFiles();
        $this->printMySqlCounts();
    }

    private function fetchMySqlFiles()
    {
        $this->files = preg_grep('/\.sql$/i', glob($this->path . '*'));
        $this->logger->log("found " . count($this->files) . " files" );
        $this->job_result["files"] = $this->files;
    }

    private function transMySqlformFiles()
    {
        foreach ($this->files as $key => $val)
        {
            try {

                $t = tempnam(sys_get_temp_dir(),"exponline");

                //  external script ignores 'drop table' statements
                preg_match_all('/^DROP TABLE(.*)$/im', file_get_contents($val), $matches);
                if (isset($matches[0]))
                {
                    file_put_contents($t,implode("\n",$matches[0]) . "\n\n");
                }

                $s = shell_exec($this->converter . ' "' . $val . '"');
                file_put_contents($t,$s,FILE_APPEND);

                $this->sqlite_files[] = ["original"=>$val,"converted"=>$t];

            } catch (Exception $e)
            {
                $this->logger->log("couldn't convert $val ($e)" ,1);
            }

        }

        $this->logger->log("converted " . count($this->sqlite_files) . " files" );
    }

    private function runSqlFiles()
    {
        foreach ($this->sqlite_files as $key => $val)
        {
            $this->db->query(file_get_contents($val["converted"]));
            $this->logger->log("imported " . $val["original"]);
        }
    }

    private function printMySqlCounts()
    {
        foreach ($this->tables as $key => $val)
        {
            $r = $this->db->querySingle("select count(*) as total from $val");
            $this->logger->log("found " . number_format($r) . " rows in " . $val);
            $this->job_result["imported"][] = [$val=>$r];
        }
    }

}