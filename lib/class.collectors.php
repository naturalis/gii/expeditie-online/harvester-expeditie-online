<?php

class Collectors extends BaseClass
{
    private $path;
    private $files;
    private $yes_values = [ "ja", "JA" ];
    private $collectors = [];
    private $imported_synonyms = 0;
    private $imported_collectors = 0;

    protected $table_name = "collector_synonyms";
    protected $table_def =
        "create table if not exists collector_synonyms (
            id                     INTEGER PRIMARY KEY  autoincrement,
            collector              varchar(255),
            synonym                varchar(255),
            inserted               timestamp not null,
            UNIQUE(collector,synonym)
        );";

    protected $sql_insert = "
        insert into collector_synonyms (
            collector, synonym, inserted
        ) values (
            :collector, :synonym, datetime('now')
        )";

    protected $job_name = "collectors";

    public function __construct ()
    {
        parent::__construct();

        $this->path = getEnv('HARVESTER_PATH_COLLECTORS',null);

        if (empty($this->path))
        {
            throw new Exception("no input folder set" ,1);
        }

        $this->path = rtrim($this->path,"/") . "/";

        if (!file_exists($this->path))
        {
            throw new Exception("input folder " . $this->path . " doesn't exist" ,1);
        }

    }

    public function runImport()
    {
        $this->fetchFiles();
        $this->readFiles();
        $this->clearTable();
        $this->insertData();
        $this->setJobResult([
            "collectors" => $this->imported_collectors,
            "synonyms" => $this->imported_synonyms,
            "file" => $this->files
        ]);
    }

    public function clearTable()
    {
        foreach ($this->collectors as $collector)
        {
            $stmt = $this->db->prepare("delete from $this->table_name where collector = :collector");
            $stmt->bindValue(':collector',$collector["collector"],SQLITE3_TEXT);
            $stmt->execute();
            $this->logger->log("cleared records for '" . $collector["collector"] . "'" );
        }
    }

    private function fetchFiles()
    {
        $this->files =  preg_grep('/\.csv$/i', glob($this->path . '*'));
        $this->logger->log("found " . count($this->files) . " files" );
    }

    private function readFiles()
    {
        foreach ($this->files as $file)
        {
            $collector = null;
            $synonyms = [];
            $lines = file($file);
            foreach($lines as $n => $line)
            {
                $cells = str_getcsv($line);
                if ($n==0)
                {
                    $collector = trim($cells[0]);
                }
                elseif ($n==1)
                {
                    continue;
                }
                else
                {
                    if (in_array($cells[2], $this->yes_values) && !empty($cells[0]))
                    {
                        $synonyms[] = trim($cells[0]);
                    }
                }
            }

            $synonyms = array_unique($synonyms);

            $this->collectors[] = [ "collector" => $collector, "synonyms" => $synonyms ];
            $this->logger->log("found collector '" . $collector . "' with " . count($synonyms) . " synonyms" );
        }

        $this->logger->log("found " . number_format(count($this->collectors)) . " collectors");

    }

    private function insertData()
    {
        foreach ($this->collectors as $collector)
        {
            foreach ($collector["synonyms"] as $synonym)
            {
                $stmt = $this->db->prepare($this->sql_insert);
                $stmt->bindValue(':collector',$collector["collector"],SQLITE3_TEXT);
                $stmt->bindValue(':synonym',$synonym,SQLITE3_TEXT);
                $stmt->execute();
                $this->imported_synonyms++;
            }

            $this->imported_collectors++;
        }

        $this->logger->log("saved " . number_format($this->imported_synonyms) ." synonyms for " . number_format($this->imported_collectors) . " collectors");
    }

}
