<?php

class XenoCanto extends BaseClass
{
    private $min_quality = 0; // 5 = best ... 1 = worst

    protected $job_name = "xenocanto";
    protected $table_name = "xenocanto";
    protected $table_def =
        "create table if not exists xenocanto (
            id                     INTEGER PRIMARY KEY  autoincrement,
            scientific_name        varchar(255),
            nomen                  varchar(255),
            sound                  text,
            inserted               timestamp not null,
            UNIQUE(nomen)
        );";

    protected $sql_insert = "
        insert or replace into xenocanto (
            scientific_name, nomen, sound, inserted
        ) values (
            :scientific_name, :nomen, :sound, datetime('now')
        )";

    private $birds_sounds = [];

    private $nba_url = 'https://api.biodiversitydata.nl/v2/multimedia/download/?_querySpec=';
    private $nba_query = '{
        "conditions": [
            {
                "field": "sourceSystem.code",
                "operator": "=",
                "value": "XC"
            },
            {
                "field": "serviceAccessPoints.format",
                "operator": "=",
                "value": "audio/mp3"
            },
            {
                "field": "rating",
                "operator": "!="
            }
        ],
        "fields": [
            "license",
            "licenseType",
            "owner",
            "recordURI",
            "serviceAccessPoints",
            "type",
            "identifications.scientificName.fullScientificName",
            "rating"
        ],
        "size": %MAX_RECORDS%
    }';

    private $nba_url_resolve = 'https://api.biodiversitydata.nl/v2/taxon/query/?_querySpec=';
    private $nba_query_resolve = '
        {
            "conditions": [
                {
                    "field": "acceptedName.scientificNameGroup",
                    "operator": "IN",
                    "value": [ %TAXA% ]
                }
            ],
            "fields": [
                "acceptedName.fullScientificName",
                "acceptedName.scientificNameGroup"
            ],
            "size": 1024
        }';

    private $quality_skipped=0;

    public function __construct ()
    {
        parent::__construct();
    }

    public function setMinQuality($min_quality)
    {
        $this->min_quality = $min_quality;
    }

    public function runImport()
    {
        $this->logger->log("max records: " . number_format($this->getMaxRecords()));
        $this->logger->log("min quality: " . number_format($this->min_quality));

        $this->getXcFromNBA();
        $this->getFullNamesFromNBA();

        if (count($this->birds_sounds)>0)
        {
            $this->clearTable();
            $this->insertData();
        }

        $this->setJobResult([
            "records" => $this->imported,
            "skipped (quality)" => $this->quality_skipped,
            "setting: max_records" => $this->getMaxRecords(),
            "setting: min_quality" => $this->min_quality,
        ]);
    }

    private function getXcFromNBA()
    {
        $handle = @fopen($this->nba_url .
            rawurlencode(str_replace('%MAX_RECORDS%',$this->getMaxRecords(),$this->nba_query)), "r");

        $i=0;

        if ($handle)
        {
            while (($raw = fgets($handle, 8092)) !== false)
            {
                $obj = json_decode($raw);

                $data = [
                    "nomen" => $obj->identifications[0]->scientificName->fullScientificName,
                    "sound" => [
                        "rating" => $obj->rating ?? "?",
                        "recordURI" => $obj->recordURI ?? "?",
                        "accessUri" => $obj->serviceAccessPoints[0]->accessUri ?? "?",
                        "format" => $obj->serviceAccessPoints[0]->format ?? "?",
                        "license" => $obj->license ?? "?",
                        "licenseType" => $obj->licenseType ?? "?",
                        "owner" => $obj->owner ?? "?"
                    ]
                ];

                $key = strtolower($data["nomen"]);

                if (
                    !isset($this->birds_sounds[$key]) ||
                    (
                        isset($this->birds_sounds[$key]) &&
                        $this->birds_sounds[$key]["sound"]["rating"] < $data["sound"]["rating"]
                    )
                )
                {
                    $this->birds_sounds[$key] = $data;
                }

                $i++;

                if ($i % 10000==0)
                {
                    $this->logger->log(number_format($i) . " documents > " . number_format(count($this->birds_sounds)) . " taxa");
                }
            }

            if (!feof($handle))
            {
                throw new Exception("Error: unexpected fgets() fail", 1);
            }

            fclose($handle);
        }

        if ($this->min_quality>0)
        {
            $pre = count($this->birds_sounds);
            $this->birds_sounds = array_filter($this->birds_sounds,function($a){ return $a["sound"]["rating"]>=$this->min_quality; });
            $this->logger->log("skipped " . number_format($pre - count($this->birds_sounds)) . " (min quality $this->min_quality)");
            $this->quality_skipped += ($pre - count($this->birds_sounds));
        }

        $this->logger->log("retrieved " . number_format(count($this->birds_sounds)) . " species groups from NBA");
    }

    private function getBufferNames($buffer)
    {
       $raw =
            file_get_contents(
                $this->nba_url_resolve . rawurlencode(
                    str_replace('%TAXA%',
                        implode(",",array_map(function($a) { return json_encode(strtolower($a)); }, $buffer))
                        ,$this->nba_query_resolve)
                )
            );

        foreach(json_decode($raw)->resultSet as $val)
        {
            // echo $val->item->acceptedName->fullScientificName,"\n";
            // echo $val->item->acceptedName->scientificNameGroup,"\n";
            $this->birds_sounds[$val->item->acceptedName->scientificNameGroup]["scientific_name"] =
                $val->item->acceptedName->fullScientificName;
        }

    }

    private function getFullNamesFromNBA()
    {
        $buffer=[];
        foreach ($this->birds_sounds as $key => $val)
        {
            $buffer[] = $val["nomen"];

            if (count($buffer)>=1024)
            {
                $this->getBufferNames($buffer);
                $buffer=[];
            }
        }

        $this->getBufferNames($buffer);
        $this->logger->log("found full scientific name for " . number_format(count($this->birds_sounds)) . " sounds");
    }

    private function insertData()
    {
        $this->imported=0;
        $this->db->exec("begin transaction");
        foreach ($this->birds_sounds as $sound)
        {
            $stmt = $this->db->prepare($this->sql_insert);
            $stmt->bindValue(':scientific_name',$sound["scientific_name"] ?? "",SQLITE3_TEXT);
            $stmt->bindValue(':nomen',$sound["nomen"],SQLITE3_TEXT);
            $stmt->bindValue(':sound',json_encode($sound["sound"]),SQLITE3_TEXT);
            $stmt->execute();
            $this->imported++;
        }
        $this->db->exec("commit");

        $this->logger->log("saved ".number_format($this->imported)." sounds");
    }
}
