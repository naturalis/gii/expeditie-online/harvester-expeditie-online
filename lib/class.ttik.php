<?php

class TTIK extends BaseClass
{
    private $url = "https://ttik.linnaeus.naturalis.nl/linnaeus_ng/app/views/webservices/";
    private $max_fetch = -1;
    protected $table_name = [
        "ttik",
        // "ttik_photo_species",
        "ttik_translations"
    ];
    protected $table_def = [
        "create table if not exists ttik (
            id                     INTEGER PRIMARY KEY  autoincrement,
            classification         text,
            uninomial              varchar(255),
            specific_epithet       varchar(255),
            infra_specific_epithet varchar(255),
            authorship             varchar(255),
            taxon                  varchar(255),
            rank                   varchar(20),
            english                text,
            dutch                  text,
            synonyms               text,
            remark                 varchar(255),
            taxon_id               int(11) not null unique,
            inserted               timestamp not null
        );",

        // "create table if not exists ttik_photo_species (
        //     id              INTEGER PRIMARY KEY  autoincrement,
        //     taxon           varchar(50),
        //     main_image      varchar(1024),
        //     inserted        timestamp not null
        // );",

        "create table if not exists ttik_translations (
            id            INTEGER PRIMARY KEY  autoincrement,
            language_code varchar(2),
            description   text,
            taxon_id      int(11) not null,
            inserted      timestamp not null,
            UNIQUE(taxon_id,language_code)
        );"
    ];

    protected $sql_insert_main = "
        insert into ttik (
            classification, uninomial, specific_epithet, infra_specific_epithet, authorship, taxon, rank,
            english, dutch, synonyms, remark, taxon_id, inserted
        ) values (
            :classification, :uninomial, :specific_epithet, :infra_specific_epithet, :authorship, :taxon, :rank, :english, :dutch, :synonyms, :remark, :taxon_id, datetime('now')
        )";

    protected $sql_insert_description = "
        insert into ttik_translations (
            language_code, description, taxon_id, inserted
        ) values (
            :language_code, :description, :taxon_id, datetime('now')
        )";

    private $rows = 1000;
    private $offset = 0;
    private $pid = 1;
    private $from = '19000101';

    private $names = [];
    private $taxa = [];

    protected $job_name = "TTIK (taxa)";

    public function __construct ()
    {
        parent::__construct();
    }

    public function runImport()
    {
        $this->fetchNames();
        $this->fetchtaxa();

        if (count($this->taxa)>0)
        {
            $this->clearTable();
            $this->insertData();
        }

        $this->setJobResult([ "records" => $this->imported ]);
    }

    private function fetchNames()
    {
        $c = 1;
        $this->names = [];

        while($c>0)
        {
            $q = implode("&", [
                "pid=$this->pid",
                "from=$this->from",
                "rows=$this->rows",
                "offset=$this->offset",
                "all=1"]);

            $result = json_decode(file_get_contents($this->url . 'names.php?' . $q ));
            $this->names = array_merge($this->names,$result->names);
            $this->offset += $this->rows;
            $c = $result->count;
            if ($c>0) $this->logger->log("fetched $c names");

            if ($this->max_fetch > 0 && count($this->names)>=$this->max_fetch)
            {
                $this->logger->log("capping on " . $this->max_fetch . " taxa");
                $this->names = array_slice($this->names,0,$this->max_fetch);
                break;
            }
        }
    }

    private function fetchtaxa()
    {
        foreach ($this->names as $name)
        {
            $id = $name->taxon_id;
            $common = [];
            $synonyms = [];

            if (!isset($this->taxa[$id])) $this->taxa[$id]=[];

            // Valid name; add main data
            if ($name->language == 'Scientific' && $name->nametype == 'isValidNameOf')
            {
                $this->taxa[$id] = array_merge($this->taxa[$id], (array)$name);
            }
            // Synonyms
            elseif ($name->language == 'Scientific')
            {
                $synonyms[] = [
                    'uninomial' => $name->uninomial,
                    'specific_epithet' => $name->specific_epithet,
                    'infra_specific_epithet' => $name->infra_specific_epithet,
                    'authorship' => $name->authorship,
                    'nametype' => $name->nametype,
                ];

                if (!empty($this->taxa[$id]['synonyms']))
                {
                    $synonyms[] = array_merge(json_decode($this->taxa[$id]['synonyms']), $synonyms);
                }

                $this->taxa[$id]['synonyms'] = json_encode($synonyms);
            }
            // Common names in Dutch or English
            elseif (in_array($name->language, ['English', 'Dutch']))
            {
                $common[] = ['name' => $name->name, 'nametype' => $name->nametype, 'remark' => $name->remark ];
                if (!empty($this->taxa[$id][strtolower($name->language)]))
                {
                    $common = array_merge(json_decode($this->taxa[$id][strtolower($name->language)]), $common);
                }
                $this->taxa[$id][strtolower($name->language)] = json_encode($common);
            }

            if (!isset($this->taxa[$id]['description']))
                $this->taxa[$id]['description'] = $this->getTaxonDescription($id);

            if (!isset($this->taxa[$id]['classification']))
                $this->taxa[$id]['classification'] = $this->getTaxonClassification($id);

            if (!isset($this->taxa[$id]['synonyms']))
                $this->taxa[$id]['synonyms'] = null;

            if (count($this->taxa) % $this->rows == 0)
            {
                $this->logger->log("fetched " . number_format(count($this->taxa)) . " taxa");
            }
        }

        $this->logger->log("fetched " . number_format(count($this->taxa)) . " taxa");

    }

    private function getTaxonDescription ($taxonId)
    {
        $description=[];

        foreach (['nl', 'en'] as $lang)
        {
            $description[$lang]=[];
            foreach ([1, 4, 5] as $cat)
            {
                $q = implode("&", [
                    "pid=$this->pid",
                    "taxon=$taxonId",
                    "cat=$cat",
                    "lang=" . ($lang=='en' ? '26' : '24'),
                    ]);

                $data = json_decode(file_get_contents($this->url . 'taxon_page.php?' . $q ));

                if (!empty($data->page->body))
                {
                    $description[$lang][] = [
                        "title" => $data->page->title,
                        "body" => $data->page->body,
                        "verified" => empty($data->page->publish) ? "0" : $data->page->publish
                    ];
                }
            }
        }

        return $description;
    }

    private function getTaxonClassification ($taxonId)
    {
        $q = implode("&", [
            "pid=$this->pid",
            "taxon=$taxonId"
         ]);

        $data = json_decode(file_get_contents($this->url . 'taxonomy.php?' . $q ));

        if (!empty($data->classification) && count($data->classification) > 0)
        {
            return json_encode($data->classification);
        }

        return null;
    }

    private function insertData()
    {
        foreach ($this->taxa as $key => $val)
        {
            $stmt = $this->db->prepare($this->sql_insert_main);
            $stmt->bindValue(':classification',$val['classification'],SQLITE3_TEXT);
            $stmt->bindValue(':uninomial',$val['uninomial'],SQLITE3_TEXT);
            $stmt->bindValue(':specific_epithet',$val['specific_epithet'],SQLITE3_TEXT);
            $stmt->bindValue(':infra_specific_epithet',$val['infra_specific_epithet'],SQLITE3_TEXT);
            $stmt->bindValue(':authorship',$val['authorship'],SQLITE3_TEXT);
            $stmt->bindValue(':taxon',$val['taxon'],SQLITE3_TEXT);
            $stmt->bindValue(':rank',$val['rank'],SQLITE3_TEXT);
            $stmt->bindValue(':english',$val['english'] ?? null,SQLITE3_TEXT);
            $stmt->bindValue(':dutch',$val['dutch'] ?? null,SQLITE3_TEXT);
            $stmt->bindValue(':synonyms',$val['synonyms'],SQLITE3_TEXT);
            $stmt->bindValue(':remark',$val['remark'],SQLITE3_TEXT);
            $stmt->bindValue(':taxon_id',$val['taxon_id'],SQLITE3_INTEGER);
            $stmt->execute();

            foreach ($val["description"] as $lang => $dsc)
            {
                if (empty($dsc))
                {
                    continue;
                }

                $stmt = $this->db->prepare($this->sql_insert_description);
                $stmt->bindValue(':language_code',$lang,SQLITE3_TEXT);
                $stmt->bindValue(':description',json_encode($dsc),SQLITE3_TEXT);
                $stmt->bindValue(':taxon_id',$val['taxon_id'],SQLITE3_INTEGER);
                $stmt->execute();
            }

            $this->imported++;
        }

        $this->logger->log("saved " . number_format($this->imported) . " records");
    }

}
