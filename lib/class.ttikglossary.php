<?php

class TTIKGlossary extends BaseClass
{
    public $path;
    public $converter;
    public $sqlite_files=[];
    // private $sql_update = "update glossary set definition = :definition where id = :id";
    public $tables = [
        "glossary",
        "glossary_synonyms",
        "media",
        "media_captions",
        "media_metadata",
        "media_modules",
    ];

    protected $job_name = "TTIK (glossary)";

    public function __construct ()
    {
        parent::__construct();

        $this->path = getEnv('HARVESTER_PATH_TTIK_GLOSSARY') ?: null;

        if (empty($this->path))
        {
            throw new Exception("no input folder set",1);
        }

        $this->path = rtrim($this->path,"/") . "/";

        if (!file_exists($this->path))
        {
            throw new Exception("input folder " . $this->path . " doesn't exist",1);
        }

        $this->converter = getEnv('HARVESTER_MYSQL_SQLITE_CONVERTER') ?: null;

        if (empty($this->converter))
        {
            throw new Exception("no path set for mysql->sqlite converter",1);
        }
    }

    public function runImport()
    {
        $this->runMySQLImport();
    }

}
