<?php

class Topstukken extends BaseClass
{
    private $index_data;
    private $data = [];
    private $url = "https://topstukken.naturalis.nl/";
    protected $table_name = "topstukken";
    protected $table_def = "
        create table if not exists topstukken (
            id                 INTEGER PRIMARY KEY  autoincrement,
            description        text,
            title              varchar(100),
            registrationNumber varchar(100) unique,
            collection         varchar(50),
            country            varchar(50),
            scientificName     varchar(100),
            year               varchar(10),
            expedition         varchar(100),
            collector          varchar(100),
            url                varchar(255),
            image              varchar(255),
            inserted           timestamp not null
        );";

    protected $sql_insert = "
        insert into topstukken (
            description, title, registrationNumber, collection, country, scientificName, year,
            expedition, collector, url, image, inserted
        ) values (
            :description, :title, :registrationNumber, :collection, :country, :scientificName, :year,
            :expedition, :collector, :url, :image, datetime('now')
        )";

    protected $job_name = "topstukken";

    public function __construct ()
    {
        parent::__construct();
    }

    public function runImport()
    {
        $this->fetchIndexData();
        $this->fetchMainData();
        if (count($this->data)>0)
        {
            $this->clearTable();
            $this->insertData();
        }
        $this->setJobResult([ "records" => $this->imported ]);
    }

    /**
     * This assumes that a script with the variable INITIAL_DATA is present containing the
     * complete dataset.
     */
    private function fetchData($url)
    {
        $var = "";

        foreach (file($url) as $line)
        {
            if (preg_match('/^(.*)var INITIAL_DATA/',$line))
            {
                $var = trim(preg_replace('/^(.*)var INITIAL_DATA =/','',$line));
            }
        }

        $f = strpos($var, '{');

        $data = json_decode(substr($var, $f, strrpos($var, '}') - $f + 1));

        $json_error = $this->getLastJsonError();

        if ($json_error)
        {
            throw new Exception("JSON parse error: $json_error", 1);
        }

        return $data;
    }

    private function fetchIndexData()
    {
        $this->index_data = $this->fetchData($this->url);
        $this->logger->log("fetched index");
    }

    private function fetchMainData()
    {
        foreach ($this->index_data->grid->items as $item)
        {
            $objectUrl = rtrim($this->url, '/') . '/object/' . $item->slug;
            $object = $this->fetchData($objectUrl);
            $title = $object->specimen->title;

            $description = [];

            $data = (array)$object->specimen->info;

            if (isset($object->specimen->blocks))
            {
                foreach ($object->specimen->blocks as $block)
                {
                    $description[] = [trim($block->title ?? ''), trim($block->body ?? '')];
                }
            }

            $data['title'] = trim($title);
            $data['description'] = json_encode($description);
            $data['url'] = trim($objectUrl);
            $data['image'] = trim(rtrim($this->url, '/') . $object->specimen->image->srcSet->{'1920'});

            $this->data[] = $data;
            $this->total++;
        }

        $this->logger->log("fetched " . number_format($this->total) . " records");
    }

    private function insertData()
    {
        foreach ($this->data as $key => $val)
        {
            $stmt = $this->db->prepare($this->sql_insert);
            $stmt->bindValue(':description',trim($val['description'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':title',trim($val['title'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':registrationNumber',trim($val['registrationNumber'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':collection',trim($val['collection'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':country',trim($val['country'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':scientificName',trim($val['scientificName'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':year,',trim($val['year'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':expedition',trim($val['expedition'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':collector',trim($val['collector'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':url',trim($val['url'] ?? ''), SQLITE3_TEXT);
            $stmt->bindValue(':image',trim($val['image'] ?? ''), SQLITE3_TEXT);
            $stmt->execute();
            $this->imported++;
        }

        $this->logger->log("saved " . number_format($this->imported) . " records");
    }
}