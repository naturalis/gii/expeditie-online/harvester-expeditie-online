<?php

class TTIKTaxa extends BaseClass
{
    public $path;
    public $converter;
    public $sqlite_files=[];
    public $tables = [
        "content_taxa",
        "languages",
        "name_types",
        "names",
        "nsr_ids",
        "pages_taxa",
        "pages_taxa_titles",
        "projects_ranks",
        "ranks",
        "taxa",
    ];

    protected $job_name = "TTIK (taxa)";

    public function __construct ()
    {
        parent::__construct();

        $this->path = getEnv('HARVESTER_PATH_TTIK_TAXA') ?: null;

        if (empty($this->path))
        {
            throw new Exception("no input folder set",1);
        }

        $this->path = rtrim($this->path,"/") . "/";

        if (!file_exists($this->path))
        {
            throw new Exception("input folder " . $this->path . " doesn't exist",1);
        }

        $this->converter = getEnv('HARVESTER_MYSQL_SQLITE_CONVERTER') ?: null;

        if (empty($this->converter))
        {
            throw new Exception("no path set for mysql->sqlite converter",1);
        }
    }

    public function runImport()
    {
        $this->runMySQLImport();
    }

}
