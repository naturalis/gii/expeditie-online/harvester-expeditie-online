<?php

class Natuurwijzer extends BaseClass
{

    private $token;
    private $curl;
    private $index_data;
    private $data = [];
    private $limit = 50;

    private $urls =
        [
            "learningobjects" =>
                "https://natuurwijzer.naturalis.nl/api/v2/learningobjects?filter[rooms][condition][path]=id&filter[rooms][condition][operator]=IS%20NOT%20NULL&page[limit]=%LIMIT%&page[offset]=%OFFSET%",
            "researchers" =>
                "https://natuurwijzer.naturalis.nl/api/v2/researchers?page[limit]=%LIMIT%&page[offset]=%OFFSET%"
        ];

    protected $table_name = "natuurwijzer";
    protected $table_def = "
        create table if not exists natuurwijzer (
            id                   INTEGER PRIMARY KEY  autoincrement,
            title                varchar(100),
            url                  varchar(100),
            taxon                varchar(1000),
            exhibition_rooms     varchar(250),
            collections          varchar(250),
            image_urls           text,
            author               varchar(100),
            intro_text           text,
            langcode             varchar(10),
            inserted             timestamp not null
        );";

    protected $sql_insert = "
        insert into natuurwijzer (
            title, url, taxon, exhibition_rooms, collections, image_urls, author, intro_text, langcode, inserted
        ) values (
            :title, :url, :taxon, :exhibition_rooms, :collections, :image_urls, :author, :intro_text, :langcode, datetime('now')
        )";

    protected $job_name = "natuurwijzer";

    public function __construct ()
    {
        parent::__construct();

        $this->token = getEnv('HARVESTER_TOKEN_NATUURWIJZER');

        if (empty($this->token))
        {
            throw new Exception("token not set", 1);
        }

        if (getEnv('HARVESTER_NATUURWIJZER_BATCH_SIZE'))
        {
            $this->limit = intval(getEnv('HARVESTER_NATUURWIJZER_BATCH_SIZE'));
            $this->logger->log("fetch batch size set to $this->limit (HARVESTER_NATUURWIJZER_BATCH_SIZE)");
        }

        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_HTTPHEADER, [
            'nw-access-token:' . $this->token,
            'Cache-Control:no-cache'
        ]);

        curl_setopt_array($this->curl,[
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        ]);
    }

    public function runImport()
    {
        $this->objects = [];

        foreach($this->urls as $category=>$url)
        {
            $this->url = $url;
            $this->category = $category;
            $this->fetchDocuments();
        }

        $this->total = count($this->objects);
        $this->logger->log("fetched $this->total records");
        if ($this->total > 0)
        {
            $this->clearTable();
            $this->insertData();
        }
        $this->setJobResult(["records" => $this->imported]);
    }

    public function fetchDocuments()
    {
        // $this->objects = [];
        $doc_count = 1;
        $offset = 0;
        $failed = 0;
        $max_fail = 100;
        $last_error = false;
        $retrieved = 0;

        while($doc_count > 0 && $failed < $max_fail)
        {
            $url = str_replace(["%LIMIT%","%OFFSET%"], [$this->limit,$offset], $this->url);
            curl_setopt($this->curl, CURLOPT_URL, $url);
            $content = curl_exec($this->curl);
            $doc = json_decode($content);

            if (!$doc)
            {
                preg_match('/<title>([^<]*)<\/title>/',$content,$matches);
                $last_error = $this->getLastJsonError() . "; page title: '" . $matches[1] . "'";
                $failed++;
            }
            else
            {
                $objects = $this->parseDocuments($doc->data);
                $this->objects += array_merge($this->objects,$objects);
                $doc_count = count($doc->data);
                $offset+=$this->limit;
                $retrieved += count($objects);
            }
        }

        $this->logger->log("fetched $retrieved records for $this->category");

        if ($failed>0)
        {
            $this->logger->log("failed $failed (quit trying after $max_fail fails); last error: \"$last_error\"");
        }
    }

    private function parseDocuments($docs)
    {
        $objects=[];

        foreach ((array)$docs as $i => $row)
        {
            // if (empty($row->attributes->taxon) && empty($row->attributes->exhibition_rooms))
            if (empty($row->attributes->taxon) && empty($row->attributes->collections))
            {
                continue;
            }

            $url = $this->category=="researchers" ? $row->attributes->path->alias : $row->attributes->url;
            $author = $this->category=="researchers" ? $row->attributes->field_author : $row->attributes->author;
            $intro_text = $this->category=="researchers" ? $row->attributes->field_intro_text->value : $row->attributes->intro_text;
            $title = $this->category=="researchers" ?
                ($row->attributes->title .
                    ($row->attributes->field_function ? ": " . $row->attributes->field_function : "")) :
                $row->attributes->title;

            $objects[$i]['title'] = trim($title);
            $objects[$i]['url'] = trim($url);
            $objects[$i]['taxon'] = isset($row->attributes->taxon) ? json_encode($row->attributes->taxon) : null;
            $objects[$i]['exhibition_rooms'] = isset($row->attributes->exhibition_rooms) ? json_encode($row->attributes->exhibition_rooms) : null;
            $objects[$i]['collections'] = $row->attributes->collections ? json_encode($row->attributes->collections) : null;
            $objects[$i]['image_urls'] = $row->attributes->image->image->urls ? json_encode($row->attributes->image->image->urls) : null;
            $objects[$i]['author'] = trim($author);
            $objects[$i]['intro_text'] = trim($intro_text);
            $objects[$i]['langcode'] = trim($row->attributes->langcode);
        }

        return $objects;
    }

    private function insertData()
    {
        $this->imported = 0;

        foreach ($this->objects as $key => $val)
        {
            $stmt = $this->db->prepare($this->sql_insert);
            $stmt->bindValue(':title',$val['title'], SQLITE3_TEXT);
            $stmt->bindValue(':url',$val['url'], SQLITE3_TEXT);
            $stmt->bindValue(':taxon',$val['taxon'], SQLITE3_TEXT);
            $stmt->bindValue(':exhibition_rooms',$val['exhibition_rooms'], SQLITE3_TEXT);
            $stmt->bindValue(':collections',$val['collections'], SQLITE3_TEXT);
            $stmt->bindValue(':image_urls',$val['image_urls'], SQLITE3_TEXT);
            $stmt->bindValue(':author',$val['author'], SQLITE3_TEXT);
            $stmt->bindValue(':intro_text',$val['intro_text'], SQLITE3_TEXT);
            $stmt->bindValue(':langcode,',$val['langcode'], SQLITE3_TEXT);
            $stmt->execute();
            $this->imported++;
        }

        $this->logger->log("saved $this->imported records");
    }

}