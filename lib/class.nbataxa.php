<?php

class NbaTaxa extends BaseClass
{
    protected $table_name = "nba_taxa";
    protected $table_def =
        "create table if not exists nba_taxa (
            id                     INTEGER PRIMARY KEY  autoincrement,
            classification         text,
            uninomial              varchar(255),
            specific_epithet       varchar(255),
            infraspecific_epithet  varchar(255),
            authorship             varchar(255),
            taxon                  varchar(255),
            rank                   varchar(20),
            inserted               timestamp not null,
            unique(taxon)
        );";

    protected $sql_insert = "
        insert or ignore into nba_taxa (
            classification, uninomial, specific_epithet, infraspecific_epithet, authorship, taxon, rank, inserted
        ) values (
            :classification, :uninomial, :specific_epithet, :infraspecific_epithet, :authorship, :taxon, :rank, datetime('now')
        )";

    protected $nba_url = 'https://api.biodiversitydata.nl/v2/specimen/download/?_querySpec=';
    protected $nba_query = '{
            "fields": [
                "collectionType",
                "identifications.taxonRank",
                "identifications.defaultClassification",
                "identifications.scientificName"
            ],
            "conditions": [
                {
                    "field": "sourceSystem.code",
                    "operator": "in",
                    "value": [
                        "CRS",
                        "BRAHMS"
                    ]
                },
                {
                    "field": "gatheringEvent.siteCoordinates.longitudeDecimal",
                    "operator": "NOT_EQUALS"
                },
                {
                    "field": "gatheringEvent.siteCoordinates.latitudeDecimal",
                    "operator": "NOT_EQUALS"
                },
                {
                    "field": "collectionType",
                    "operator": "NOT_IN",
                    "value": [ "Petrology", "Mineralogy and Petrology", "Mineralogy" ]
                },
                {
                    "field": "identifications.taxonRank",
                    "operator": "in",
                    "value": [
                        "species",
                        "subspecies",
                        "variety",
                        "var.",
                        "subsp.",
                        "forma",
                        "cv.",
                        "f.",
                        "subvar",
                        "subvar.",
                        "subf.",
                        "ssp.",
                        "var"
                    ]
                }
            ]
        }';

    private $ignorableSubstrings = [ " spec.", " sp.", " indet", "?" ];
    private $buffer_size = 50000;

    private $classifications = [];
    private $identifications_read = 0;
    private $docs_read = 0;
    private $identifications_retrieved = 0;
    private $identifications_saved = 0;

    protected $job_name = "NBA";

    public function __construct ()
    {
        parent::__construct();
    }

    public function runImport()
    {
        $this->logger->log("starting harvest");

        $this->openConnection();
        $this->clearTable();
        $this->getClassifications();

        fclose($this->handle);

        $this->setJobResult([
            "documents" => $this->docs_read,
            "identifications read" => $this->identifications_read,
            "identifications retrieved" => $this->identifications_retrieved,
            "identifications saved" => $this->identifications_saved
        ]);

        $this->logger->log("done");
    }

    private function openConnection()
    {
        $this->handle = @fopen($this->nba_url . rawurlencode($this->nba_query), "r");
    }

    private function getClassifications()
    {
        if ($this->handle)
        {
            while (($raw = fgets($this->handle, 8092)) !== false)
            {
                $doc = json_decode($raw,true);
                // var_dump($doc);

                $this->docs_read++;

                if (!isset($doc["identifications"]))
                {
                    $this->logger->log("no identifications or empty scientific name: " . @$doc["id"]);
                    continue;
                }

                foreach ($doc["identifications"] as $key => $val)
                {
                    $this->identifications_read++;

                    if (!isset($val["taxonRank"]))
                    {
                        // $this->logger->log("no taxonRank: " . $doc["id"] . " (" . $doc["collectionType"] .")");
                        continue;
                    }

                    $fsn = $val["scientificName"]["fullScientificName"];

                    $process = true;

                    foreach ($this->ignorableSubstrings as $iss)
                    {
                        if (str_contains($fsn, $iss))
                        {
                            // $this->logger->log("skipping: " . $fsn);
                            $process = false;
                            break;
                        }
                    }

                    if (!$process)
                    {
                        continue;
                    }

                    if (isset($val["defaultClassification"]))
                    {
                        if (!isset($this->classifications[$fsn]))
                        {
                            $this->classifications[$fsn] = [
                                "classification" => $val["defaultClassification"],
                                "name" => [
                                    "rank" => $val["taxonRank"],
                                    "fullScientificName" => $val["scientificName"]["fullScientificName"],
                                    "genusOrMonomial" => $val["scientificName"]["genusOrMonomial"] ?? null,
                                    "specificEpithet" => $val["scientificName"]["specificEpithet"] ?? null,
                                    "infraspecificEpithet" => $val["scientificName"]["infraspecificEpithet"] ?? null,
                                    "authorshipVerbatim" => $val["scientificName"]["authorshipVerbatim"] ?? null,
                                ]
                            ];

                            $this->identifications_retrieved++;
                        }
                    }
                }

                if (count($this->classifications)>=$this->buffer_size)
                {
                    $this->progressFeedback();
                    $this->insertData();
                    unset($this->classifications);
                }
            }
        }

        if (count($this->classifications)>0)
        {
            $this->progressFeedback();
            $this->insertData();
            unset($this->classifications);
        }
    }

    private function progressFeedback()
    {
        $this->logger->log("read " .
            number_format($this->identifications_read) . " identifcations from " .
            number_format($this->docs_read) . " docs, extracted " .
            number_format($this->identifications_retrieved) . " valid"
        );
    }

    private function insertData()
    {
        $this->db->exec("begin transaction");
        foreach ($this->classifications as $val)
        {
            $c = [];
            $r = "";
            $t = "";
            $genus = null;
            $specificEpithet = null;

            foreach($val["classification"] as $rank => $val2)
            {
                $t = $val2;

                switch ($rank)
                {
                    case 'kingdom':
                        $r = 'regnum';
                        break;
                    case 'phylum':
                        $r = 'phylum';
                        break;
                    case 'className':
                        $r = 'classis';
                        break;
                    case 'order':
                        $r = 'ordo';
                        break;
                    case 'family':
                        $r = 'familia';
                        break;
                    case 'genus':
                        $r = 'genus';
                        $genus = $val2;
                        break;
                    case 'specificEpithet':
                        $r = 'species';
                        $specificEpithet = $val2;
                        $t = trim($genus . " " . $specificEpithet);
                        break;
                    case 'infraspecificEpithet':
                        $r = 'subspecies';
                        $t = trim($genus . " " . $specificEpithet . " " . $val2);
                        break;
                    default:
                        $r = $rank;
                        break;
                }

                $c[]=["taxon"=>$t,"rank"=>$r];
            }

            $stmt = $this->db->prepare($this->sql_insert);
            $stmt->bindValue(':classification',json_encode($c),SQLITE3_TEXT);
            $stmt->bindValue(':uninomial',$val["name"]["genusOrMonomial"] ?? "" ,SQLITE3_TEXT);
            $stmt->bindValue(':specific_epithet',$val["name"]["specificEpithet"] ?? "" ,SQLITE3_TEXT);
            $stmt->bindValue(':infraspecific_epithet',$val["name"]["infraspecificEpithet"] ?? "" ,SQLITE3_TEXT);
            $stmt->bindValue(':authorship',$val["name"]["authorshipVerbatim"] ?? "" ,SQLITE3_TEXT);
            $stmt->bindValue(':taxon',$val["name"]["fullScientificName"] ?? "" ,SQLITE3_TEXT);
            $stmt->bindValue(':rank',$val["name"]["rank"] ?? "" ,SQLITE3_TEXT);

            if ($stmt->execute())
            {
                $this->identifications_saved++;
            }
        }

        $this->db->exec("commit");
        $this->logger->log("saved " . number_format($this->identifications_saved) . " records");
    }
}
