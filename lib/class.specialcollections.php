<?php

class SpecialCollections extends BaseClass
{
    protected $table_name = "special_collections_ids";
    protected $table_def =
        "create table if not exists special_collections_ids (
            id          INTEGER PRIMARY KEY  autoincrement,
            collection  varchar(32),
            unitid      varchar(32),
            inserted    timestamp not null,
            UNIQUE(collection,unitid)
        );";

    protected $sql_insert = "
        insert into special_collections_ids (
            collection, unitid, inserted
        ) values (
            :collection, :unitid, datetime('now')
        )";

    protected $nba_url = 'https://api.biodiversitydata.nl/v2/specimen/getDistinctValues/theme/?_querySpec=%7B%20%22size%22%20%3A%2099%20%7D';
    protected $nba_url_detail = 'https://api.biodiversitydata.nl/v2/specimen/download/?_querySpec=';
    protected $nba_query_detail = '{
        "conditions": [
            {
                "field": "theme",
                "operator": "=",
                "value": "%THEME%"
            }
        ],
        "fields": [
            "sourceSystemId"
        ],
        "size": %MAX_RECORDS%
    }';

    private $records_saved = [];
    protected $themes=[];
    protected $special_collections = [];
    protected $job_name = "special collections";

    public function __construct ()
    {
        parent::__construct();
    }

    public function runImport()
    {
        $this->getThemesFromNBA();
        $this->getUnitIdsFromNBA();
        if (count($this->special_collections)>0)
        {
            $this->clearTable();
            $this->insertData();
        }
        $this->setJobResult([ "records saved" => $this->records_saved ]);
    }

    private function getThemesFromNBA()
    {
        $handle = @fopen($this->nba_url, "r");

        $i=0;

        if ($handle)
        {
            while (($raw = fgets($handle, 8092)) !== false)
            {
                $this->themes = json_decode($raw,true);
            }

            fclose($handle);
        }

        $this->logger->log("retrieved " . number_format(count($this->themes)) . " themes from NBA");
    }

    private function getUnitIdsFromNBA()
    {
        foreach($this->themes as $theme => $count)
        {

            $unitids = [];

            $handle = @fopen(
                $this->nba_url_detail .
                rawurlencode(str_replace(['%THEME%','%MAX_RECORDS%'],[$theme,$count],$this->nba_query_detail)), "r");


            if ($handle)
            {
                while (($raw = fgets($handle, 8092)) !== false)
                {
                    $obj = json_decode($raw);
                    $unitids[] = $obj->sourceSystemId;
                }

                if (!feof($handle))
                {
                    throw new Exception("Error: unexpected fgets() fail", 1);
                }

                fclose($handle);

                $this->special_collections[$theme] = $unitids;
                $this->logger->log("retrieved " . number_format(count($unitids)) . " unitIDs for " . $theme);
            }
        }
    }

    private function insertData()
    {
        $this->db->exec("begin transaction");
        foreach ($this->special_collections as $theme => $unitids)
        {
            $this->records_saved[$theme]=0;

            foreach ($unitids as $unitid)
            {
                $stmt = $this->db->prepare($this->sql_insert);
                $stmt->bindValue(':collection',$theme,SQLITE3_TEXT);
                $stmt->bindValue(':unitid',$unitid,SQLITE3_TEXT);
                $stmt->execute();
                $this->records_saved[$theme]++;
            }
        }
        $this->db->exec("commit");
        $this->logger->log("saved " . number_format(count($this->records_saved)) . " themes");
    }
}
