<?php

    $source = getenv('HARVESTER_SOURCE') ?: exit(0);

    include_once("lib/class.logger.php");
    include_once("lib/class.baseClass.php");
    include_once("lib/class.topstukken.php");
    include_once("lib/class.natuurwijzer.php");
    // include_once("lib/class.ttik.php");
    include_once("lib/class.ttiktaxa.php");
    include_once("lib/class.ttikglossary.php");
    include_once("lib/class.collectors.php");
    include_once("lib/class.nsr.php");
    include_once("lib/class.wikispecies.php");
    include_once("lib/class.xenocanto.php");
    include_once("lib/class.specialcollections.php");
    include_once("lib/class.videos.php");
    include_once("lib/class.nbataxa.php");

    try {

        $logger = new Logger();
        $logger->setFile(getenv('LOGFILE_PATH') ?: null);

        switch ($source)
        {
            case "topstukken":
                $c = new Topstukken();
                break;

            case "natuurwijzer":
                $c = new Natuurwijzer();
                break;

            case "collectors":
                $c = new Collectors();
                break;

            case "nsr":
                $c = new NSR();
                break;

            case "wikispecies":
                $c = new WikiSpecies();
                break;

            case "ttik":
                // $c = new TTIK();
                $c = new TTIKTaxa();
                break;

            case "ttik_glossary":
                $c = new TTIKGlossary();
                break;

            case "xenocanto":
                $c = new XenoCanto();
                $c->setMaxRecords(2000000);
                $c->setMinQuality(3); // 3 = A,B,C
                break;

            case "special_collections":
                $c = new SpecialCollections();
                break;

            case "nba_taxa":
                $c = new NbaTaxa();
                $c->setMaxRecords(2000000);
                break;

            // case "videos":
            //     $c = new Videos();
            //     break;

            default:
                throw new Exception("unknown harvest source: $source", 1);
                break;
        }

        $c->logger->setFile(getenv('LOGFILE_PATH') ?: null);
        $c->runImport();
        $c->logImport();

    } catch (Exception $e)
    {

        $logger->setCallingClassOverride("Harvester ($source)");
        $logger->log($e->getMessage(),1);

    }
